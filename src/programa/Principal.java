package programa;

import java.util.Scanner;
import clases.Fusil;

public class Principal {
	public static Scanner input = new Scanner(System.in);
	public static void main(String[] args) {
		/**
		 * 
		 * Creamos el menu general, hemos tomado varios menus pero antes delimitamos
		 * el total de piezas
		 * 
		 * */
		boolean fin = false;
		System.out.println("Bienvenido a la creaci�n de armamento NorKor sponsorizados por telemierda, \nno podemos hacer armas nucleares, pero haremos mierda a nuestros enemigos");
		System.out.println("Primero de todo selecciona la cantidad de chasis que tienes");
		int cantidadChasis = input.nextInt();
		System.out.println("Segundo selecciona la cantidad de ca�ones que tienes");
		int cantidadCanones = input.nextInt();
		System.out.println("Tercero selecciona la cantidad de culatas que tienes");
		int cantidadCulatas = input.nextInt();
		System.out.println("Cuarto selecciona la cantidad de cargadores que tienes");
		int cantidadCargadores = input.nextInt();
		System.out.println("Quinto selecciona la cantidad de mirillas que tienes");
		int cantidadMirillas = input.nextInt();
		Fusil rifle = new Fusil(cantidadCanones, cantidadCargadores, cantidadChasis, cantidadCulatas, cantidadMirillas);
		/*
		 * Hacemos una comprobaci�n previa para la ejecuci�n del programa, de modo que siempre tenga que ser el total de piezas superior a 1
		 * 
		 * */
		if(rifle.totalChasis()>0 || (rifle.totalCanones()!=0 && rifle.totalCargadores()!=0 && rifle.totalChasis()!=0 && rifle.totalCulata()!=0 && rifle.totalMirilla()!=0)) {
			do {
				System.out.println("****************************************************");
				System.out.println("|                  Opciones:                       |");
				System.out.println("|           1- Dar de alta pieza                   |");
				System.out.println("|           2- Listar                              |");
				System.out.println("|           3- Comprobar compatibilidades          |");
				System.out.println("|           4- Mejores piezas                      |");
				System.out.println("|           5- Combinaciones                       |");
				System.out.println("|           6- Borrar una pieza                    |");
				System.out.println("|           7- Editar una pieza                    |");
				System.out.println("|           8- Buscar componente                   |");
				System.out.println("|           9- Salir                               |");
				System.out.println("****************************************************");
				int opcion = input.nextInt();
		/**
		 * 
		 * Sacamos las primeras opciones en las que tenemos el men� principal
		 * 
		 * */
				switch(opcion) {
				case 1:
					boolean end1 = false;
					do {
		/**
		 * De aqu� obtenemos las opciones que va a tener el submen� para a�adir piezas por separado donde llamar� a cada opcion a los m�todos correspondientes
		 * En todas ellass se piden los par�metros que se van a introducir y posteriormente se ponen llamando a un m�todo de la clase rifle.
		 * Para comprobar que hay hueco para a�adir y poner cuantos puedes meter lo hago con 2 metodos, el que us� previamente que devuelve la cantididad de 
		 * piezas que hay disponibles en el vector, y posteriormente con otro m�todo huecoCanones, por ejemplo, hago devolver las casillas que no est�n ocupadas
		 * de modo que si ese n�mero es 0 pues no va a poner ninguna pieza m�s. Tambi�n con el m�todo estaElCanon hago que la pieza si se encuentra en el array
		 * no la ponga, dado que ya est�, y consideramos que se pueden modificar las piezas, pero no tener repetidas. Esto es sobre todo para evitar problemas con 
		 * las marcas y los modelos, porque el usuario podr�a ponerlos a mano, y tampoco creo que tenga que conocer la posici�n en la que se encuentra cada uno de los datos
		 * 
		 * */
						
						System.out.println("****************************************************");
						System.out.println("|                  Opciones:                       |");
						System.out.println("|           1- Alta Ca�on                          |");
						System.out.println("|           2- Alta Chasis                         |");
						System.out.println("|           3- Alta Culata                         |");
						System.out.println("|           4- Alta Cargador                       |");
						System.out.println("|           5- Alta Mirilla                        |");
						System.out.println("|           6- Salir                               |");
						System.out.println("****************************************************");
						int option1 = input.nextInt();
					switch(option1) {
					case 1:
						if(rifle.huecoCanones()!=0) {
							System.out.println("Puedes a�adir "+rifle.huecoCanones()+" de "+rifle.totalCanones());
							System.out.println("Introduce la velocidad de escape");
							double velocidadEscape = input.nextDouble();
							System.out.println("Introduce la dispersion del ca�on");
							double dispersion = input.nextDouble();
							System.out.println("Introduce peso");
							double peso = input.nextDouble();
							System.out.println("Introduce el modelo");
							String modelo = input.next();
							System.out.println("Introduce marca");
							String marca = input.next();
							System.out.println("Introduce medida");
							double medida = input.nextDouble();
							if(rifle.estaElCanon(marca, modelo)>=0) {
								System.out.println("No puedes poner ese producto camarada, ya lo has puesto");
							}else {
								rifle.altaCanon(velocidadEscape, dispersion, peso, modelo, marca, medida);													
							}
						}else {
							System.out.println("Ya no puedes meter m�s");
						}
						break;
					case 2:
						if(rifle.huecoChasis()!=0) {
							System.out.println("Puedes a�adir "+rifle.huecoChasis()+" de "+rifle.totalChasis());
							System.out.println("Introduce la velocidad de escape");
							double velocidadEscape = input.nextDouble();
							System.out.println("Introduce la dispersion del ca�on");
							double dispersion = input.nextDouble();
							System.out.println("Introduce peso");
							double peso = input.nextDouble();
							System.out.println("Introduce el modelo");
							String modelo = input.next();
							System.out.println("Introduce marca");
							String marca = input.next();
							System.out.println("Introduce punteria");
							double punteria = input.nextDouble();
							System.out.println("Introduce estabilidad");
							double estabilidad = input.nextDouble();
							System.out.println("Introduce medida del cargador");
							double medidaCargador = input.nextDouble();
							System.out.println("Introduce medida ca�on");
							double medidaCanon = input.nextDouble();
							System.out.println("Introduce medida culata");
							double medidaCulata = input.nextDouble();
							System.out.println("Introduce medida mirilla");
							double medidaMirilla = input.nextDouble();
							if(rifle.estaElChasis(marca, modelo)>=0) {
								System.out.println("No puedes poner ese producto camarada, ya lo has puesto");								
							}else {
								rifle.altaChasis(peso, velocidadEscape, dispersion, punteria, estabilidad, medidaCanon, medidaCargador, medidaCulata, medidaMirilla, modelo, marca);				
							}
						}else {
							System.out.println("Ya no puedes meter m�s");
						}
						break;
					case 3:
						if(rifle.huecoCulatas()!=0) {
							System.out.println("Puedes a�adir "+rifle.huecoCulatas()+" de "+rifle.totalCulata());
							System.out.println("Introduce estabilidad");
							double estabilidad = input.nextDouble();
							System.out.println("Introduce peso");
							double peso = input.nextDouble();
							System.out.println("Introduce medida");
							double medida = input.nextDouble();
							System.out.println("Introduce modelo");
							String modelo = input.next();
							System.out.println("Introduce marca");
							String marca = input.next();
							if(rifle.estaElCulata(marca, modelo)>=0) {
								System.out.println("No puedes poner ese producto camarada, ya lo has puesto");
							}else {
								rifle.altaCulata(estabilidad, peso, medida, modelo, marca);
							}
						}else {
							System.out.println("Ya no puedes meter m�s");
						}
						break;
					case 4:
						if(rifle.huecoCargadores()!=0) {
							System.out.println("Puedes a�adir "+rifle.huecoCargadores()+" de "+rifle.totalCargadores());
							System.out.println("Introuce tama�o cargador");
							int cargadorTam = input.nextInt();
							System.out.println("Introduce estabilidad");
							double estabilidad = input.nextDouble();
							System.out.println("Introduce peso");
							double peso = input.nextDouble();
							System.out.println("Introduce medida");
							double medida = input.nextDouble();
							System.out.println("Introduce modelo");
							String modelo = input.next();
							System.out.println("Introduce marca");
							String marca = input.next();
							if(rifle.estaElCargador(marca, modelo)>=0) {
								System.out.println("No puedes poner ese producto camarada, ya lo has puesto");
							}else {
								rifle.altaCargador(cargadorTam, estabilidad, peso, medida, modelo, marca);
							}
						}else {
							System.out.println("Ya no puedes meter m�s");
						}
						break;
					case 5:
						if(rifle.huecoMirillas()!=0) {
							System.out.println("Puedes a�adir "+rifle.huecoMirillas()+" de "+rifle.totalMirilla());
							System.out.println("Introduce precision");
							double precision = input.nextDouble();
							System.out.println("Introduce peso");
							double peso = input.nextDouble();
							System.out.println("Introduce medida");
							double medida = input.nextDouble();
							System.out.println("Introduce modelo");
							String modelo = input.next();
							System.out.println("Introduce marca");
							String marca = input.next();
							if(rifle.estaElMirilla(marca, modelo)>=0) {
								System.out.println("No puedes poner ese producto camarada, ya lo has puesto");

							}else {								
								rifle.altaMirilla(precision, peso, medida, modelo, marca);
							}
						}else {
							System.out.println("Ya no puedes meter m�s");
						}
						break;
					case 6:
						System.out.println("Saliste con �xito camarada");
						end1=true;
						break;
					default:
						System.out.println("Opcion no contemplada camarada, prueba con alguna de las establecidas por nuestro armsiticio");
						}
					}while(end1==false);
					break;
				case 2:
	/**
	 * No hay gran complejidad en estos, lo �nico que es es un m�todo que te saca las piezas que se han guardado
	 * */
					boolean end2=false;
					do {
						System.out.println("*****************************************************");
						System.out.println("|                  Opciones:                        |");
						System.out.println("|           1- Listar Todo                          |");
						System.out.println("|           2- Listar Ca�ones                       |");
						System.out.println("|           3- Listar Chasis                        |");
						System.out.println("|           4- Listar Culatas                       |");
						System.out.println("|           5- Listar Mirillas                      |");
						System.out.println("|           6- Salir                                |");
						System.out.println("*****************************************************");
						int option2 = input.nextInt();
						switch(option2) {
						case 1:
							System.out.println("Sus componentes:");
							rifle.listarTodo();
							break;
						case 2:
							System.out.println("Sus ca�ones");
							rifle.listarCanones();
							break;
						case 3:
							System.out.println("Sus chasis");
							rifle.listarChasis();
							break;
						case 4:
							System.out.println("Sus culatas");
							rifle.listarCulata();
							break;
						case 5:
							System.out.println("Sus mirillas");
							rifle.listarMirillas();
							break;
						case 6: 
							System.out.println("Saliste con �xito camarada");
							end2=true;
							break;
						default:
							System.out.println("Opcion no contemplada camarada, prueba con alguna de las establecidas por nuestro armsiticio");
						}
					}while(end2==false);
					break;
				case 3:
					boolean end3=false;
			/*
			 * Te devuleve un booelano en caso de que haya alguna pieza que coincida en medidas con un chasis
			 * 
			 * */
					do {
						System.out.println("*****************************************************");
						System.out.println("|                  Opciones:                        |");
						System.out.println("|           1- Compatibilidad Ca��n                 |");
						System.out.println("|           2- Compatibilidad Culata                |");
						System.out.println("|           3- Compatibilidad Cargador              |");
						System.out.println("|           4- Compatibilidad Mirilla               |");
						System.out.println("|           5- Salir                                |");
						System.out.println("*****************************************************");
						int option3=input.nextInt();
						switch(option3) {
						case 1:
							System.out.println("�Hay chasis para alg�n ca�on? "+rifle.cabeCanonEnChasis());
							break;
						case 2: 
							System.out.println("�Hay chasis para alguna culata? "+rifle.cabeCulataEnChasis());
							break;
						case 3: 
							System.out.println("�Hay chasis para alguna cargador? "+rifle.cabeCargadorEnChasis());
							break;
						case 4: 
							System.out.println("�Hay chasis para alguna mirilla? "+rifle.cabeMirillaEnChasis());
							break;
						case 5:
							System.out.println("Saliste con �xito camarada");
							end3=true;
							break;
						default:
							System.out.println("Opcion no contemplada camarada, prueba con alguna de las establecidas por nuestro armsiticio");
							break;
						}
					}while(end3==false);
					break;
				case 4:
					boolean end4=false;
			/**
			 * reordena el vector de las piezas para poner las piezas ordenadas de la mejor a la peor. Se emplea el m�todo de la burbuja y se retorna la posici�n 0
			 * */
					do {
						System.out.println("*****************************************************");
						System.out.println("|                  Opciones:                        |");
						System.out.println("|           1- Mejor Ca��n                          |");
						System.out.println("|           2- Mejor Culata                         |");
						System.out.println("|           3- Mejor Cargador                       |");
						System.out.println("|           4- Mejor Mirilla                        |");
						System.out.println("|           5- Mejor Chasis                         |");
						System.out.println("|           6- Salir                                |");
						System.out.println("*****************************************************");
						int option4 = input.nextInt();
						switch(option4) {
						case 1:
							System.out.println("El ca��n m�s potente que tienes es ");
							rifle.canonMasPotente();
							break;
						case 2:
							System.out.println("La culata m�s estable que tienes es ");
							rifle.culataMasEstable();
							break;
						case 3:
							System.out.println("El cargador que m�s balas tiene y m�s estable es ");
							rifle.culataMasEstable();
							break;
						case 4:
							System.out.println("La mirilla m�s precisa es ");
							rifle.mirillaMasPrecisa();
							break;
						case 5:
							System.out.println("El mejor chasis que tienes es ");
							rifle.bestChasis();
							break;
						case 6:
							System.out.println("Saliste con �xito camarada");
							end4=true;
							break;
						default:
							System.out.println("Opcion no contemplada camarada, prueba con alguna de las establecidas por nuestro armsiticio");
							break;
						}
						
					}while(end4==false);
					break;
				case 5:
					boolean end5=false;
					/**
					 *Lista todas las combinaciones posibles, ello lo hace a trav�s de 3 pasos: 1� contabiliza la cantidad de coincidencias que hay entre el chasis
					 *y el atributo que se busca que es la medida. Una vez sacado el total de coincidencias se pone una condici�n para que en caso de que sea 0 no 
					 *haga nada, posteriormente si es mayor a 0 devolver� la componenete y el chasis sacando todas las combinaciones. Primero lo que hace es llenar 
					 *el array de los candidatos, y posteriormente sacar� la lista de los candidatos ordenada. 
					 * */
					do {
						System.out.println("*****************************************************");
						System.out.println("|                  Opciones:                        |");
						System.out.println("|           1- Combinaciones Chasis-Ca��n           |");
						System.out.println("|           2- Combinaciones Chasis-Culata          |");
						System.out.println("|           3- Combinaciones Chasis-Cargador        |");
						System.out.println("|           4- Combinaciones Chasis-Mirilla         |");
						System.out.println("|           5- Salir                                |");
						System.out.println("*****************************************************");
						int opcion5 = input.nextInt();
						switch(opcion5) {
						case 1:
							rifle.riflePotentePosibles();
							break;
						case 2:
							rifle.rifleEstablePosibles();
							break;
						case 3:
							rifle.rifleCargadorPosibles();
							break;
						case 4:
							rifle.riflePrecisosPosibles();
							break;
						case 5:
							System.out.println("Saliste con �xito camarada");
							end5=true;
							break;
						default:
							System.out.println("Opcion no contemplada camarada, prueba con alguna de las establecidas por nuestro armsiticio");
							break;
						}
					}while(end5==false);
					break;
				case 6:
					boolean end6=false;
				/**
				 * Simple le metes una serie de par�metros que son marca y modelo y de ah� pues te borra la compoenente designada
				 * 
				 * */
					do {
						System.out.println("******************************************************");
						System.out.println("|                  Opciones:                         |");
						System.out.println("|           1- Borrar Ca��n                          |");
						System.out.println("|           2- Borrar Culata                         |");
						System.out.println("|           3- Borrar Cargador                       |");
						System.out.println("|           4- Borrar Mirilla                        |");
						System.out.println("|           5- Borrar Chasis                         |");
						System.out.println("|           6- Salir                                 |");
						System.out.println("******************************************************");
						int opcion6 = input.nextInt();
						switch(opcion6) {
						case 1:
							System.out.println("Pon marca del ca�on que quieres borrar");
							String marcaCan = input.next();
							System.out.println("Pon modelo del ca�on que quieres borrar");
							String modeloCan = input.next();
							rifle.borrarCanon(marcaCan, modeloCan);
							break;
						case 2:
							System.out.println("Pon marca de la culata que quieres borrar");
							String marcaCul = input.next();
							System.out.println("Pon modelo de la culata que quieres borrar");
							String modeloCul = input.next();
							rifle.borrarCulata(marcaCul, modeloCul);
							break;
						case 3:
							System.out.println("Pon marca del cargador que quieres borrar");
							String marcaCar = input.next();
							System.out.println("Pon modelo del cargador que quieres borrar");
							String modeloCar = input.next();
							rifle.borrarCulata(marcaCar, modeloCar);
							break;
						case 4:
							System.out.println("Pon marca de la mirilla que quieres borrar");
							String marcaMir = input.next();
							System.out.println("Pon modelo de la mirilla que quieres borrar");
							String modeloMir = input.next();
							rifle.borrarCulata(marcaMir, modeloMir);
							break;
						case 5:
							System.out.println("Pon marca del chasis que quieres borrar");
							String marcaCha = input.next();
							System.out.println("Pon modelo del chasis que quieres borrar");
							String modeloCha = input.next();
							rifle.borrarCulata(marcaCha, modeloCha);
							break;
						case 6:
							System.out.println("Saliste con �xito camarada");
							end6=true;
							break;
						default:
							System.out.println("Opcion no contemplada camarada, prueba con alguna de las establecidas por nuestro armsiticio");
							break;
						}
					}while(end6==false);
					break;
				case 7:
					boolean end7=false;
					do {
						/*
						 * 
						 * Lo que hacemos aqu� es pedir una cantidad de par�metros para poder editar los stats de los diversos componentes. Lo que 
						 * tenemos con esto es que das a la opci�n y de ah� le das a los par�metros que quieres editar, de modo que al final los cambia.
						 * */
						System.out.println("******************************************************");
						System.out.println("|                  Opciones:                         |");
						System.out.println("|           1- Editar Ca��n                          |");
						System.out.println("|           2- Editar Culata                         |");
						System.out.println("|           3- Editar Cargador                       |");
						System.out.println("|           4- Editar Mirilla                        |");
						System.out.println("|           5- Editar Chasis                         |");
						System.out.println("|           6- Salir                                 |");
						System.out.println("******************************************************");
						int opcion7 = input.nextInt();
						switch(opcion7) {
						case 1:
							System.out.println("Introduce Marca del Ca�on");
							String marcaCan = input.next();
							System.out.println("Introduce Modelo del Ca�on");
							String modeloCan = input.next();
							System.out.println("Introduce nueva Velocidad de Escape");
							double velocidadEscapeCan = input.nextDouble();
							System.out.println("Introduce nueva Dispersi�n");
							double dispersionCan = input.nextDouble();
							System.out.println("Introduce nuevo Peso");
							double pesoCan = input.nextDouble();
							System.out.println("Introduce nueva Medida");
							double medidaCan = input.nextDouble();
							rifle.actualizarCanon(marcaCan, modeloCan, velocidadEscapeCan, dispersionCan, pesoCan, medidaCan);
							break;
						case 2:
							System.out.println("Introduce Marca de la Culata");
							String marcaCul = input.next();
							System.out.println("Introduce Modelo de la Culata");
							String modeloCul = input.next();
							System.out.println("Introduce la nueva Estabilidad de la Culata");
							double estabilidadCul = input.nextDouble();
							System.out.println("Introduce el nuevo Peso de la Culata");
							double pesoCul = input.nextDouble();
							System.out.println("Introduce la nueva Medida de la Culata");
							double medidaCul = input.nextDouble();
							rifle.actualizarCaulata(marcaCul, modeloCul, estabilidadCul, pesoCul, medidaCul);
							break;
						case 3:
							System.out.println("Introduce marca del Cargador");
							String marcaCar = input.next();
							System.out.println("Introduce el modelo del Cargador");
							String modeloCar = input.next();
							System.out.println("Introduce la nueva Medida del Cargador");
							double medidaCar = input.nextDouble();
							System.out.println("Introduce el nuevo Peso del Cargador");
							double pesoCar = input.nextDouble();
							System.out.println("Introduce la nueva Estabilidad del Cargador");
							double estabilidadCar = input.nextDouble();
							System.out.println("Introduce la nueva Cantidad de balas que porta el Cargaodor");
							int cargadorTamCar = input.nextInt();
							rifle.actualizarCargador(cargadorTamCar, estabilidadCar, pesoCar, medidaCar, modeloCar, marcaCar);
							break;
						case 4:
							System.out.println("Mirilla");
							System.out.println("Introduce la marca de la Mirilla");
							String marcaMir = input.next();
							System.out.println("Introduce el modelo de la Mirilla");
							String modeloMir = input.next();
							System.out.println("Introduce la nueva Precision de la Mirilla");
							double precisionMir = input.nextDouble();
							System.out.println("Introduce el nuevo Peso de la Mirilla");
							double pesoMir = input.nextDouble();
							System.out.println("Introduce la nueva Medida de la Mirilla");
							double medidaMir = input.nextDouble();
							rifle.actualizarMirilla(marcaMir, modeloMir, precisionMir, pesoMir, medidaMir);
							break;
						case 5:
							System.out.println("Chasis");
							System.out.println("Introduce la marca del Chasis");
							String marca = input.next();
							System.out.println("Introduce el modelo del Chasis");
							String modelo = input.next();
							System.out.println("Introduce el nuevo Peso del Chasis");
							double peso = input.nextDouble();
							System.out.println("Introduce la nueva Velocidad de Escape");
							double velocidadEscape = input.nextDouble();
							System.out.println("Introduce nueva Dispersion");
							double dispersion = input.nextDouble();
							System.out.println("Introduce nueva Punter�a");
							double punteria = input.nextDouble();
							System.out.println("Introduce nueva Estabilidad");
							double estabilidad = input.nextDouble();
							System.out.println("Introduce nueva MedidaCanon");
							double medidaCanon = input.nextDouble();
							System.out.println("Introduce nueva MedidaCargador");
							double medidaCargador = input.nextDouble();
							System.out.println("Introduce nueva MedidaCulata");
							double medidaCulata = input.nextDouble();
							System.out.println("Introduce nueva MedidaMirilla");
							double medidaMirilla = input.nextDouble();
							rifle.actualizarChasis(marca, modelo, peso, velocidadEscape, dispersion, punteria, estabilidad, medidaCanon, medidaCargador, medidaCulata, medidaMirilla);
							break;
						case 6:
							System.out.println("Saliste con �xito camarada");
							end7=true;
							break;
						}
					}while(end7==false);
					break;
				case 8:
					boolean end8=false;
					do {
						/*
						 * Este m�todo lo que hace es buscar por marca y modelo, y te devolver� 1 resultado correspondiente a lo que has buscado
						 * */
						System.out.println("******************************************************");
						System.out.println("|                  Opciones:                         |");
						System.out.println("|           1- Buscar Ca��n                          |");
						System.out.println("|           2- Buscar Culata                         |");
						System.out.println("|           3- Buscar Cargador                       |");
						System.out.println("|           4- Buscar Mirilla                        |");
						System.out.println("|           5- Buscar Chasis                         |");
						System.out.println("|           6- Salir                                 |");
						System.out.println("******************************************************");
						int opcion7 = input.nextInt();
						String marca;
						String modelo;
						switch(opcion7) {
						case 1:
							System.out.println("Pon la marca del ca�on");
							marca = input.next();
							System.out.println("Pon el modelo del ca�on");
							modelo = input.next();
							rifle.encontrarCanon(marca, modelo);
							break;
						case 2:
							System.out.println("Pon la marca de la culata");
							marca = input.next();
							System.out.println("Pon el modelo de la culata");
							modelo = input.next();
							rifle.encontrarCulata(marca, modelo);
							break;
						case 3:
							System.out.println("Pon la marca del cargador");
							marca = input.next();
							System.out.println("Pon el modelo del cargador");
							modelo = input.next();
							rifle.encontrarCargador(marca, modelo);
							break;
						case 4:
							System.out.println("Pon la marca de la mirilla");
							marca = input.next();
							System.out.println("Pon el modelo de la mirilla");
							modelo = input.next();
							rifle.encontrarMirilla(marca, modelo);
							break;
						case 5:
							System.out.println("Pon la marca del chasis");
							marca = input.next();
							System.out.println("Pon el modelo del chasis");
							modelo = input.next();
							rifle.encontrarChasis(marca, modelo);
							break;
						case 6:
							System.out.println("Saliste con �xito camarada");
							end8=true;
							break;
						}
					}while(end8==false);
					break;
				case 9:
					System.out.println("Ve a la guerra camarada.... ����Estas preparado!!!!");
					fin=true;
					break;
				default:
					System.out.println("La opci�n no esta en el menu");
					break;
				}
			}while(fin==false);
			input.close();
		}else {
			System.out.println("No puedes hacer rifles sin chasis, todo lo dem�s puede prescindir en detrimento de m�s stats vuelve a correr el programa");
		}
	}
		
}
