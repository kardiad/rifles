package clases;

public class Fusil {
	/**
	 * @author: Jafet N��ez Gonz�lez
	 * @version: 2.0
	 * 
	 * */
	private Canon [] canones;
	private Cargador[] cargador;
	private Chasis[] chasis;
	private Culata[] culata;
	private Mirilla[] mirilla;
	/**
	 * @Override
	 * @param canones
	 * @param cargador
	 * @param chasis
	 * @param culata
	 * @param otros
	 */
	public Fusil(int maxCanon, int maxCargador, int maxChasis, int maxCulata, int maxMirilla) {
		canones=new Canon[maxCanon];
		cargador=new Cargador[maxCargador];
		chasis=new Chasis[maxChasis];
		culata=new Culata[maxCulata];
		mirilla=new Mirilla[maxMirilla];
	}
	/**
	 * 
	 * cantidad total de algo
	 * @returns int
	 * 
	 * */
	
	public int totalChasis() {
		return chasis.length;
	}
	
	public int totalCanones() {
		return canones.length;
	}
	
	public int totalCargadores() {
		return cargador.length;
	}
	
	public int totalMirilla() {
		return mirilla.length;
	}
	
	public int totalCulata() {
		return culata.length;
	}
	
	/**
	 * setters y getters
	 * @return canones: devuelve vector canones
	 * @return cargador: devuelve vector cargador 
	 * @return chasis: devuelve vector chasis
	 * @return mirilla: devuelve vector mirilla
	 * @return culata: devuelve vector culata
	 * */
	public Canon[] getCanones() {
		return canones;
	}

	public void setCanones(Canon[] canones) {
		this.canones = canones;
	}

	public Cargador[] getCargador() {
		return cargador;
	}

	public void setCargador(Cargador[] cargador) {
		this.cargador = cargador;
	}

	public Chasis[] getChasis() {
		return chasis;
	}

	public void setChasis(Chasis[] chasis) {
		this.chasis = chasis;
	}

	public Culata[] getCulata() {
		return culata;
	}

	public void setCulata(Culata[] culata) {
		this.culata = culata;
	}

	public Mirilla[] getMirilla() {
		return mirilla;
	}

	public void setMirilla(Mirilla[] mirilla) {
		this.mirilla = mirilla;
	}
	/**
	 * 
	 * m�todos para saber si hay hueco libre
	 * @param cant : devuelve la cantidad de huecos que hay libres en cada vector.
	 * 
	 * */
	public int huecoCanones() {
		int cant = 0;
		for(int i=0; i<canones.length; i++) {
			if(canones[i]==null) {
				cant++;
			}
		}
		return cant;
	}
	
	public int huecoCulatas() {
		int cant = 0;
		for(int i=0; i<culata.length; i++) {
			if(culata[i]==null) {
				cant++;
			}
		}
		return cant;
	}
	
	public int huecoCargadores() {
		int cant = 0;
		for(int i=0; i<cargador.length; i++) {
			if(cargador[i]==null) {
				cant++;
			}
		}
		return cant;
	}
	
	public int huecoChasis() {
		int cant = 0;
		for(int i=0; i<chasis.length; i++) {
			if(chasis[i]==null) {
				cant++;
			}
		}
		return cant;
	}
	
	public int huecoMirillas() {
		int cant = 0;
		for(int i=0; i<mirilla.length; i++) {
			if(mirilla[i]==null) {
				cant++;
			}
		}
		return cant;
	}
	
	/** 
	 * metodos para dar de alta
	 * */
	
	public void altaCanon(double velocidadEscape, double dispersion, double peso, String modelo, String marca, double medida) {
		for(int i=0; i<canones.length; i++) {
			if(canones[i]==null && velocidadEscape>0 && peso>0 && medida>0) {
				canones[i]= new Canon();
				canones[i].setVelocidadEscape(velocidadEscape);
				canones[i].setDispersion(dispersion);
				canones[i].setPeso(peso);
				canones[i].setModelo(modelo);
				canones[i].setMarca(marca);
				canones[i].setMedida(medida);
				System.out.println("Canon guardado con exito");
				break;
			}
		}
	}
	
	public void altaCargador(int cargadorTam, double estabilidad, double peso, double medida, String modelo, String marca) {
		for(int i=0; i<cargador.length; i++) {
			if(cargador[i]==null && cargadorTam>0 && peso>0 && medida>0) {
				cargador[i] = new Cargador();
				cargador[i].setCargador(cargadorTam);
				cargador[i].setEstabilidad(estabilidad);
				cargador[i].setPeso(peso);
				cargador[i].setMedida(medida);
				cargador[i].setModelo(modelo);
				cargador[i].setMarca(marca);
				System.out.println("Cargador guardado con exito");
				break;
			}
		}
	}
	
	public void altaChasis(double peso, double velocidadEscape, double dispersion, double punteria, double estabilidad,
			double medidaCanon, double medidaCargador, double medidaCulata, double medidaMirilla, String modelo,
			String marca) {
		for(int i=0; i<chasis.length; i++) {
			if(chasis[i]==null && velocidadEscape>0 && medidaCanon>0 && medidaCargador>0 && medidaCulata>0 && medidaMirilla>0) {
				chasis[i] = new Chasis();
				chasis[i].setDispersion(dispersion);
				chasis[i].setEstabilidad(estabilidad);
				chasis[i].setMarca(marca);
				chasis[i].setMedidaCanon(medidaCanon);
				chasis[i].setMedidaCargador(medidaCargador);
				chasis[i].setMedidaCulata(medidaCulata);
				chasis[i].setMedidaMirilla(medidaMirilla);
				chasis[i].setModelo(modelo);
				chasis[i].setPeso(peso);
				chasis[i].setPunteria(punteria);
				chasis[i].setVelocidadEscape(velocidadEscape);
				System.out.println("Chasis guardado con exito");
				break;
			}
		}
	}
	
	public void altaCulata(double estabilidad, double peso, double medida, String modelo, String marca) {
		for(int i=0; i<culata.length; i++) {
			if(culata[i]==null && peso>0 && medida>0) {
				culata[i] = new Culata();
				culata[i].setEstabilidad(estabilidad);
				culata[i].setPeso(peso);
				culata[i].setMedida(medida);
				culata[i].setModelo(modelo);
				culata[i].setMarca(marca);
				System.out.println("Culata guardada con exito");
				break;
			}
		}
		System.out.println("");
	}
	
	public void altaMirilla(double precision, double peso, double medida, String modelo, String marca) {
		for(int i=0; i<mirilla.length; i++) {
			if(mirilla[i]==null && peso>0 && medida>0) {
				mirilla[i] = new Mirilla();
				mirilla[i].setPrecision(precision);
				mirilla[i].setPeso(peso);
				mirilla[i].setMedida(medida);
				mirilla[i].setModelo(modelo);
				mirilla[i].setMarca(marca);
				System.out.println("Mirilla guardada con exito");
				break;
			}
		}
	}
	
	/**
	 * 
	 * M�todos para ver si �sta el producto en la lista
	 * @return: en todos ellos se devuelve la posici�n 
	 * en la que se encuentra el objeto que cumple con los par�metros pedidos, 
	 * que son marca y modelo, en caso de no existir se pone un -1
	 * @param: los par�metros que se piden son 2 cadenas de texto que obedecen
	 * a la marca y al modelo
	 * */
	
	public int estaElCanon(String marca, String modelo) {
		for(int i=0; i<canones.length; i++) {
			if(canones[i]==null) {
				
			}else {
				if(canones[i].getMarca().equals(marca) && canones[i].getModelo().equals(modelo)) {
					return i;
				}							
			}
		}
		return -1;
	}
	
	
	public int estaElCargador(String marca, String modelo) {
		for(int i=0; i<cargador.length; i++) {
			if(cargador[i]==null) {
				
			}else {
				if(cargador[i].getMarca().equals(marca) && cargador[i].getModelo().equals(modelo)) {
					return i;
				}							
			}
		}
		return -1;
	}
	
	public int estaElChasis(String marca, String modelo) {
		for(int i=0; i<chasis.length; i++) {
			if(chasis[i]==null) {
				
			}else {
				if(chasis[i].getMarca().equals(marca) && chasis[i].getModelo().equals(modelo)) {
					return i;
				}
			}		
		}
		return -1;
	}
	
	public int estaElCulata(String marca, String modelo) {
		for(int i=0; i<culata.length; i++) {
			if(culata[i]==null) {
				
			}else {
				if(culata[i].getMarca().equals(marca) && culata[i].getModelo().equals(modelo)) {
					return i;
				}							
			}
		}
		return -1;
	}
	
	public int estaElMirilla(String marca, String modelo) {
		for(int i=0; i<mirilla.length; i++) {
			if(mirilla[i]==null) {
				
			}else {
				if(mirilla[i].getMarca().equals(marca) && mirilla[i].getModelo().equals(modelo)) {
					return i;
				}							
			}
		}
		return -1;
	}
	
	/**
	 * M�todos para validar que un chasis admite un componente espec�fico
	 * @return: devuelve un booleano para indicar si existe alg�n elemento que pueda 
	 * anexionarse a un chasis. De ser verdadero, podr�a usarse en la funci�n de combinar
	 * 
	 * */
	
	public boolean cabeCulataEnChasis() {
		for(int i=0; i<chasis.length; i++) {
			for(int j=0; j<culata.length; j++) {
				if(chasis[i].getMedidaCulata()==culata[j].getMedida()) {
					return true;
				}
			}
		}
		return false;
	}
	
	public boolean cabeCanonEnChasis() {
		for(int i=0; i<chasis.length; i++) {
			for(int j=0; j<canones.length; j++) {
				if(chasis[i].getMedidaCanon()==canones[j].getMedida()) {
					return true;
				}
			}
		}
		return false;
	}
	
	public boolean cabeMirillaEnChasis() {
		for(int i=0; i<chasis.length; i++) {
			for(int j=0; j<mirilla.length; j++) {
				if(chasis[i].getMedidaMirilla()==mirilla[j].getMedida()) {
					return true;
				}
			}
		}
		return false;
	}
	
	public boolean cabeCargadorEnChasis() {
		for(int i=0; i<chasis.length; i++) {
			for(int j=0; j<cargador.length; j++) {
				if(chasis[i].getMedidaCargador()==cargador[j].getMedida()) {
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * 
	 * Listados
	 * 
	 * */
	
	public void listarMirillas() {
		for(int i=0; i<mirilla.length; i++) {
			System.out.println(mirilla[i]);
		}
	}
	public void listarCargador() {
		for(int i=0; i<cargador.length; i++) {
			System.out.println(cargador[i]);
		}
	}
	public void listarChasis() {
		for(int i=0; i<chasis.length; i++) {
			System.out.println(chasis[i]);
		}
	}
	public void listarCanones() {
		for(int i=0; i<canones.length; i++) {
			System.out.println(canones[i]);
		}
	}
	public void listarCulata() {
		for(int i=0; i<culata.length; i++) {
			System.out.println(culata[i]);
		}
	}
	public void listarTodo() {
		System.out.println("Mirillas");
		System.out.println("*****************************************");
		listarMirillas();
		System.out.println("Cargadores");
		System.out.println("*****************************************");
		listarCargador();
		System.out.println("Chasis");
		System.out.println("*****************************************");
		listarChasis();
		System.out.println("Ca�ones");
		System.out.println("*****************************************");
		listarCanones();
		System.out.println("Culatas");
		System.out.println("*****************************************");
		listarCulata();
		System.out.println("*****************************************");
		System.out.println("******AH� LO LLEVAS CAMARADA*************");
	}
	
	/**
	 * 
	 * Obtener las mejores piezas
	 * 
	 * */
	
	public void mirillaMasPrecisa() {
		if(this.huecoMirillas()==0 && this.totalMirilla()>0) {
			Mirilla mir = new Mirilla();
			for(int i=0; i<mirilla.length; i++) {
				for(int j=0; j<mirilla.length; j++) {
					if(mirilla[i].getPrecision()>mirilla[j].getPrecision()) {
						mir=mirilla[i];
						mirilla[j]=mirilla[i];
						mirilla[j]=mir;
					}
				}
			}
			System.out.println(mirilla[0]);
		}else {
			System.out.println("Necesitamos que nos llenes todas las mirillas");
		}	
	}
	
	public void canonMasPotente() {
		if(this.huecoCanones()==0 && this.totalCanones()>0) {
			Canon can = new Canon();
			for(int i=0; i<canones.length; i++) {
				for(int j=0; j<canones.length; j++) {
					if(canones[i].getVelocidadEscape()>canones[j].getVelocidadEscape()) {
						can=canones[i];
						canones[i]=canones[j];
						canones[j]=can;
					}
				}
			}
			System.out.println(canones[0]);			
		}else {
			System.out.println("Necesitamos que nos llenes todos los ca�ones");
			
		}
	}
	
	public void culataMasEstable() {
		if(this.huecoCulatas()==0 && this.totalCulata()>0) {
			Culata cul = new Culata();
			for(int i=0; i<culata.length; i++) {
				for(int j=0; j<culata.length; j++) {
					if(culata[i].getEstabilidad()>culata[j].getEstabilidad()) {
						cul=culata[i];
						culata[i]=culata[j];
						culata[j]=cul;
					}
				}
			}
			System.out.println(culata[0]);			
		}else {
			System.out.println("Necesitamos que llenes todas las culatas");
		}
	}
	
	public void bestChasis() {
		if(this.huecoChasis()==0 && this.totalChasis()>0) {
			Chasis cha = new Chasis();
			for(int i=0; i<chasis.length; i++) {
				for(int j=0; j<chasis.length; j++) {
					if((chasis[i].getEstabilidad()>chasis[j].getEstabilidad()) && (chasis[i].getVelocidadEscape()>chasis[j].getVelocidadEscape()) && (chasis[i].getPunteria()>chasis[j].getPunteria())) {
						cha=chasis[i];
						chasis[i]=chasis[j];
						chasis[j]=cha;
					}
				}
			}
			System.out.println(chasis[0]);			
		}else {
			System.out.println("Necesitamos que llenes todos los chasis");
		}
	}
	
	public void mejorCargador() {
		if(this.huecoCargadores()==0 && this.totalCargadores()>0) {
			Cargador car = new Cargador();
			for(int i=0; i<cargador.length; i++) {
				for(int j=0; j<cargador.length; j++) {
					if((cargador[i].getEstabilidad()<cargador[j].getEstabilidad()) && (cargador[i].getCargador()<cargador[j].getCargador())) {
						car=cargador[i];
						cargador[i]=cargador[j];
						cargador[j]=car;
					}
				}
			}
			System.out.println(cargador[0]);			
		}else {
			System.out.println("Necesitamos que llenes todos los cargadores");
		}
	}
	
	/**
	 * 
	 * Listado de combinaciones entre las piezas y el chasis
	 * 
	 * */
	
	public void riflePrecisosPosibles() {
		/*Busco las mirillas que cabr�an en los chasis para posteriormente buscar las mejores combinaciones*/
		int cantidadRifles=0;
		for(int i=0; i<chasis.length; i++) {
			for(int j=0; j<mirilla.length; j++) {
				if(chasis[i].getMedidaMirilla()==mirilla[j].getMedida()) {
					cantidadRifles++;
					
				}				
			}
		}
		System.out.println("Total coincidencias "+cantidadRifles);
		if(cantidadRifles>0) {
			int cont = 0;
			Chasis[] chasisCandidatos = new Chasis[cantidadRifles];
			Mirilla[] mirillasCandidatas = new Mirilla[cantidadRifles];
			for(int i=0; i<chasis.length; i++) {
				for(int j=0; j<mirilla.length; j++) {
					if(chasis[i].getMedidaMirilla()==mirilla[j].getMedida() && chasis[i]!=null && mirilla[j]!=null) {
						chasisCandidatos[cont]=chasis[i];
						mirillasCandidatas[cont]=mirilla[j];
						cont++;
					}				
				}
			}
			/*Listo las combinaciones posibles*/
			for(int i=0; i<chasisCandidatos.length; i++) {
				for(int j=0; j<mirillasCandidatas.length; j++) {
					if(mirillasCandidatas[j].getMedida()==chasisCandidatos[i].getMedidaMirilla() && mirillasCandidatas[j]!=null && chasisCandidatos[i]!=null) {
						System.out.println("Combinacion posible "+i);
						System.out.println(mirillasCandidatas[j]);
						System.out.println(chasisCandidatos[i]);
					}
				}
			}			
		}else {
			System.out.println("No puedo montar rifles con los chasis y mirillas dadas");
		}
	}
	
	public void riflePotentePosibles() {
		/*Busco los ca�ones que cabr�an en los chasis para posteriormente buscar las mejores combinaciones*/
		int cantidadRifles=0;
		for(int i=0; i<chasis.length; i++) {
			for(int j=0; j<canones.length; j++) {
				if(chasis[i].getMedidaCanon()==canones[j].getMedida()) {
					cantidadRifles++;
				}				
			}
		}
		System.out.println("Total coincidencias "+cantidadRifles);
		if(cantidadRifles>0) {
			int cont = 0;
			Chasis[] chasisCandidatos = new Chasis[cantidadRifles];
			Canon[] canonesCandidatas = new Canon[cantidadRifles];
			for(int i=0; i<chasis.length; i++) {
				for(int j=0; j<canones.length; j++) {
					if(chasis[i].getMedidaCanon()==canones[j].getMedida() && chasis[i]!=null && canones[j]!=null) {
						chasisCandidatos[cont]=chasis[i];
						canonesCandidatas[cont]=canones[j];
						cont++;
					}				
				}
			}
			/*Listo las combinaciones posibles*/
			for(int i=0; i<chasisCandidatos.length; i++) {
				for(int j=0; j<canonesCandidatas.length; j++) {
					if(chasisCandidatos[i].getMedidaCanon()==canonesCandidatas[j].getMedida() && chasisCandidatos[i]!=null && canonesCandidatas[j]!=null) {
						System.out.println("Combinacion posible "+i);
						System.out.println(canonesCandidatas[j]);
						System.out.println(chasisCandidatos[i]);						
					}
				}
			}			
		}else {
			System.out.println("No puedo montar rifle con los chasis y ca�ones dados");
		}
	}
	
	public void rifleEstablePosibles() {
		/*Busco las culatas que cabr�an en los chasis para posteriormente buscar las mejores combinaciones*/
		int cantidadRifles=0;
		for(int i=0; i<chasis.length; i++) {
			for(int j=0; j<culata.length; j++) {
				if(chasis[i].getMedidaCulata()==culata[j].getMedida()) {
					cantidadRifles++;					
				}	
			}
		}
		System.out.println("Total coincidencias "+cantidadRifles);
		if(cantidadRifles>0) {
			int cont = 0;
			Chasis[] chasisCandidatos = new Chasis[cantidadRifles];
			Culata[] culatasCandidatas = new Culata[cantidadRifles];
			for(int i=0; i<chasis.length; i++) {
				for(int j=0; j<culata.length; j++) {
					if(chasis[i].getMedidaCulata()==culata[j].getMedida() && chasis[i]!=null && culata[j]!=null) {
						chasisCandidatos[cont]=chasis[i];
						culatasCandidatas[cont]=culata[j];
						cont++;
					}			
				}
			}
			/*Listo las combinaciones posibles*/
			for(int i=0; i<chasisCandidatos.length; i++) {
				for(int j=0; j<culatasCandidatas.length; j++) {
					if(culatasCandidatas[j].getMedida()==chasisCandidatos[i].getMedidaCulata() && culatasCandidatas[j]!=null && chasisCandidatos[i]!=null) {
						System.out.println("Combinacion posible "+i);
						System.out.println(culatasCandidatas[j]);
						System.out.println(chasisCandidatos[i]);
					}
				}
			}			
		}else {
			System.out.println("No puedo montar rifle con los chasis y culatas dadas");
		}
	}
	
	public void rifleCargadorPosibles() {
		/*Busco cargadores que cabr�an en los chasis para posteriormente buscar las mejores combinaciones*/
		int cantidadRifles=0;
		for(int i=0; i<chasis.length; i++) {
			for(int j=0; j<cargador.length; j++) {
				if(chasis[i].getMedidaCargador()==cargador[j].getMedida()) {
					cantidadRifles++;					
				}
			}
		}
		System.out.println("Total coincidencias "+cantidadRifles);
		if(cantidadRifles>0) {
			int cont =0;
			Chasis[] chasisCandidatos = new Chasis[cantidadRifles];
			Cargador[] cargadorCandidatos = new Cargador[cantidadRifles];
			for(int i=0; i<chasis.length; i++) {
				for(int j=0; j<cargador.length; j++) {
					if(chasis[i].getMedidaCargador()==cargador[j].getMedida() && chasis[i]!=null && cargador[j]!=null) {
						chasisCandidatos[cont]=chasis[i];
						cargadorCandidatos[cont]=cargador[j];
						cont++;
					}				
				}
			}
			/*Listo las combinaciones posibles*/
			for(int i=0; i<chasisCandidatos.length; i++) {
				for(int j=0; j<cargadorCandidatos.length; j++) {
					if(chasisCandidatos[i].getMedidaCargador()==cargadorCandidatos[j].getMedida() && chasisCandidatos[i]!=null && cargadorCandidatos[j]!=null) {
						System.out.println("combinaci�n "+i);
						System.out.println(chasisCandidatos[i]);
						System.out.println(cargadorCandidatos[j]);						
					}
				}
			}			
		}else {
			System.out.println("No puedo montar rifle con los chasis y cargadores dados");
		}
	}
	/**
	 * 
	 * M�todos para borrar pieza.
	 * @param hace uso de marca y modelo para poder detectar que borrar
	 * 
	 * */
	public void borrarCanon(String marca, String modelo) {
		int posicion = this.estaElCanon(marca, modelo);
		if(posicion>=0) {
			System.out.println("Ca��n en posicion "+posicion+" borrado con �xito");
			canones[posicion]=null;
		}else {
			System.out.println("No puedo hacer milagros y borrar lo que no existe");
		}
		
	}
	
	public void borrarChasis(String marca, String modelo) {
		int posicion = this.estaElChasis(marca, modelo);
		if(posicion>=0) {
			System.out.println("Ca��n en posicion "+posicion+" borrado con �xito");
			chasis[posicion]=null;
		}else {
			System.out.println("No puedo hacer milagros y borrar lo que no existe");
		}
	}
	
	public void borrarMirilla(String marca, String modelo) {
		int posicion = this.estaElMirilla(marca, modelo);
		if(posicion>=0) {
			System.out.println("Ca��n en posicion "+posicion+" borrado con �xito");
			mirilla[posicion]=null;
		}else {
			System.out.println("No puedo hacer milagros y borrar lo que no existe");
		}
	}
	
	public void borrarCargador(String marca, String modelo) {
		int posicion = this.estaElCargador(marca, modelo);
		if(posicion>=0) {
			System.out.println("Ca��n en posicion "+posicion+" borrado con �xito");
			cargador[posicion]=null;
		}else {
			System.out.println("No puedo hacer milagros y borrar lo que no existe");
		}
	}
	
	public void borrarCulata(String marca, String modelo) {
		int posicion = this.estaElCulata(marca, modelo);
		if(posicion>=0) {
			System.out.println("Ca��n en posicion "+posicion+" borrado con �xito");
			culata[posicion]=null;
		}else {
			System.out.println("No puedo hacer milagros y borrar lo que no existe");
		}
	}
	
	/**
	 * 
	 * M�todos para actualizar un componente.
	 * @param: se piden tanto marca como modelo para buscar el componenete del que 
	 * se quieren editar las cosas, posteriormente los dem�s par�metros se piden
	 * para cambiar las estad�sticas de cada componente. 
	 * 
	 * */
	
	public void actualizarCanon(String marca, String modelo, double velocidadEscape, double dispersion, double peso, double medida) {
		int posicion = this.estaElCanon(marca, modelo);
		if(velocidadEscape>0 && peso>0 && medida>0 && posicion>=0) {					
			canones[posicion].setDispersion(dispersion);
			canones[posicion].setVelocidadEscape(velocidadEscape);
			canones[posicion].setPeso(peso);
			System.out.println("Par�metros actualizados camarada, puedes ir a la guerra de manera m�s o menos eficiente");
		}else {
			System.out.println("Par�metros no actualizables");
		}
	}
	public void actualizarChasis(String marca, String modelo, double peso, double velocidadEscape, double dispersion, double punteria, double estabilidad,
			double medidaCanon, double medidaCargador, double medidaCulata, double medidaMirilla) {
		int posicion = this.estaElChasis(marca, modelo);
		if(velocidadEscape>0 && medidaCanon>0 && medidaCargador>0 && medidaCulata>0 && medidaMirilla>0 && posicion>=0) {
			chasis[posicion].setMedidaCanon(medidaCanon);
			chasis[posicion].setMedidaCargador(medidaCargador);
			chasis[posicion].setMedidaCulata(medidaCulata);
			chasis[posicion].setMedidaMirilla(medidaMirilla);
			chasis[posicion].setVelocidadEscape(velocidadEscape);
			System.out.println("Par�metros actualizados camarada, puedes ir a la guerra de manera m�s o menos eficiente");
		}else {
			System.out.println("Par�metros no actualizables");
		}
		
	}
	public void actualizarMirilla(String marca, String modelo, double precision, double peso, double medida) {
		int posicion = this.estaElMirilla(marca, modelo);
		if(peso>0 && medida>0 && posicion>=0) {
			mirilla[posicion].setMedida(medida);
			mirilla[posicion].setPeso(peso);
			System.out.println("Par�metros actualizados camarada, puedes ir a la guerra de manera m�s o menos eficiente");
		}else {
			System.out.println("Par�metros no actualizables");
		}
	}	
	public void actualizarCargador(int cargadorTam, double estabilidad, double peso, double medida, String modelo, String marca) {
		int posicion = this.estaElCargador(marca, modelo);
		if(cargadorTam>0 && peso>0 && medida>0 && posicion>=0) {
			cargador[posicion].setCargador(cargadorTam);
			cargador[posicion].setPeso(peso);
			cargador[posicion].setMedida(medida);
			System.out.println("Par�metros actualizados camarada, puedes ir a la guerra de manera m�s o menos eficiente");
		}else {
			System.out.println("Par�metros no actualizables");
		}
	}
	public void actualizarCaulata(String marca, String modelo, double estabilidad, double peso, double medida) {
		int posicion = this.estaElCulata(marca, modelo);
		if(peso>0 && medida>0 && posicion>=0) {
			cargador[posicion].setPeso(peso);
			cargador[posicion].setMedida(medida);
			System.out.println("Par�metros actualizados camarada, puedes ir a la guerra de manera m�s o menos eficiente");
		}else {
			System.out.println("Par�metros no actualizables");
		}
	}
	
	/**
	 * 
	 * M�todos para encontrar un componente en espec�fico por marca y modelo
	 * @param Se piden marca y modelo para encontrar un compoenente espec�fico
	 * */
	public void encontrarCanon(String marca, String modelo) {
		for(int i=0; i<canones.length; i++) {
			if(canones[i].getMarca().equals(marca) && canones[i].getModelo().equals(modelo)) {
				System.out.println("El ca�on que buscas es: "+canones[i]);
			}
		}
	}
	public void encontrarCargador(String marca, String modelo) {
		for(int i=0; i<cargador.length; i++) {
			if(cargador[i].getMarca().equals(marca) && cargador[i].getModelo().equals(modelo)) {
				System.out.println("El ca�on que buscas es: "+cargador[i]);
			}
		}
	}
	public void encontrarCulata(String marca, String modelo) {
		for(int i=0; i<culata.length; i++) {
			if(culata[i].getMarca().equals(marca) && culata[i].getModelo().equals(modelo)) {
				System.out.println("El ca�on que buscas es: "+culata[i]);
			}
		}
	}
	public void encontrarChasis(String marca, String modelo) {
		for(int i=0; i<chasis.length; i++) {
			if(chasis[i].getMarca().equals(marca) && chasis[i].getModelo().equals(modelo)) {
				System.out.println("El ca�on que buscas es: "+chasis[i]);
			}
		}
	}
	public void encontrarMirilla(String marca, String modelo) {
		for(int i=0; i<mirilla.length; i++) {
			if(mirilla[i].getMarca().equals(marca) && mirilla[i].getModelo().equals(modelo)) {
				System.out.println("El ca�on que buscas es: "+mirilla[i]);
			}
		}
	}
}
