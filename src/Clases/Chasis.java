package clases;

public class Chasis {
	private double peso;
	private double velocidadEscape;
	private double dispersion;
	private double punteria;
	private double estabilidad;
	private double medidaCanon;
	private double medidaCargador;
	private double medidaCulata;
	private double medidaMirilla;
	private String modelo;
	private String marca;
	
	/**
	 * 
	 * Constructor
	 */
	public Chasis() {
		super();
	}
	public double getPeso() {
		return peso;
	}
	public void setPeso(double peso) {
		this.peso = peso;
	}
	public double getVelocidadEscape() {
		return velocidadEscape;
	}
	public void setVelocidadEscape(double velocidadEscape) {
		this.velocidadEscape = velocidadEscape;
	}
	public double getDispersion() {
		return dispersion;
	}
	public void setDispersion(double dispersion) {
		this.dispersion = dispersion;
	}
	public double getPunteria() {
		return punteria;
	}
	public void setPunteria(double punteria) {
		this.punteria = punteria;
	}
	public double getEstabilidad() {
		return estabilidad;
	}
	public void setEstabilidad(double estabilidad) {
		this.estabilidad = estabilidad;
	}
	public double getMedidaCanon() {
		return medidaCanon;
	}
	public void setMedidaCanon(double medidaCanon) {
		this.medidaCanon = medidaCanon;
	}
	public double getMedidaCargador() {
		return medidaCargador;
	}
	public void setMedidaCargador(double medidaCargador) {
		this.medidaCargador = medidaCargador;
	}
	public double getMedidaCulata() {
		return medidaCulata;
	}
	public void setMedidaCulata(double medidaCulata) {
		this.medidaCulata = medidaCulata;
	}
	public double getMedidaMirilla() {
		return medidaMirilla;
	}
	public void setMedidaMirilla(double medidaMirilla) {
		this.medidaMirilla = medidaMirilla;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	@Override
	public String toString() {
		return "Chasis [peso=" + peso + "\n velocidadEscape=" + velocidadEscape + "\n dispersion=" + dispersion
				+ "\n punteria=" + punteria + "\n estabilidad=" + estabilidad + "\n medidaCanon=" + medidaCanon
				+ "\n medidaCargador=" + medidaCargador + "\n medidaCulata=" + medidaCulata + "\n medidaMirilla="
				+ medidaMirilla + "\n modelo=" + modelo + "\n marca=" + marca + "]";
	}
	
	
	
}
