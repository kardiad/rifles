package clases;

public class Canon {
	private double velocidadEscape;
	private double dispersion;
	private double peso;
	private String modelo;
	private String marca;
	private double medida;
	
	/**
	 * @Override
	 * Constructor
	 */
	public Canon() {
		super();
	}
	public double getVelocidadEscape() {
		return velocidadEscape;
	}
	public void setVelocidadEscape(double velocidadEscape) {
		this.velocidadEscape = velocidadEscape;
	}
	public double getDispersion() {
		return dispersion;
	}
	public void setDispersion(double dispersion) {
		this.dispersion = dispersion;
	}
	public double getPeso() {
		return peso;
	}
	public void setPeso(double peso) {
		this.peso = peso;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public double getMedida() {
		return medida;
	}
	public void setMedida(double medida) {
		this.medida = medida;
	}
	@Override
	public String toString() {
		return "Canon [velocidadEscape=" + velocidadEscape + "\n dispersion=" + dispersion + "\n peso=" + peso
				+ "\n modelo=" + modelo + "\n marca=" + marca + "\n medida=" + medida + "]";
	}
	
	
}
