package clases;

public class Cargador {
	private int cargadorTam;
	private double estabilidad;
	private double peso;
	private double medida;
	private String modelo;
	private String marca;
	
	/**
	 * @Override
	 * Constructor
	 */
	public Cargador() {
		super();
	}
	public int getCargador() {
		return cargadorTam;
	}
	public void setCargador(int cargador) {
		this.cargadorTam = cargador;
	}
	public double getEstabilidad() {
		return estabilidad;
	}
	public void setEstabilidad(double estabilidad) {
		this.estabilidad = estabilidad;
	}
	public double getPeso() {
		return peso;
	}
	public void setPeso(double peso) {
		this.peso = peso;
	}
	public double getMedida() {
		return medida;
	}
	public void setMedida(double medida) {
		this.medida = medida;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	@Override
	public String toString() {
		return "Cargador [cargador=" + cargadorTam + "\n estabilidad=" + estabilidad + "\n peso=" + peso + "\n medida="
				+ medida + "\n modelo=" + modelo + "\n marca=" + marca + "]";
	}
	
	
	
}
